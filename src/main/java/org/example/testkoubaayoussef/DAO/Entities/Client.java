package org.example.testkoubaayoussef.DAO.Entities;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Client")
@Builder
public class Client {

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Menu> menu;

    @Id
    @Column(name="idClient")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idClient;

    private String identifiant;
    private LocalDate datePremiereVisite;
}

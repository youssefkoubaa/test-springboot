package org.example.testkoubaayoussef.DAO.Entities;


import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Composant")
@Builder
public class Composant {


        @ManyToOne
        private Menu menu;


    @Id
    @Column(name="idComposant")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idComposant;

    private String nomComposant;
    private float prix;

}

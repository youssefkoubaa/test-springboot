package org.example.testkoubaayoussef.DAO.Entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Menu")
@Builder
public class Menu {

    @ManyToOne
    private Restaurant restaurant;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Client> client;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menu")
    private Set<Composant> composant;

    @Id
    @Column(name = "idMenu")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idMenu;

    private String libelleMenu;
    private TypeMenu typeMenu;
    private float prixTotal;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menu")
    private List<Composant> composants;

}

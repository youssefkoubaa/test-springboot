package org.example.testkoubaayoussef.DAO.Entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Resturant")
@Builder
public class Restaurant {


    @OneToMany(cascade = CascadeType.ALL, mappedBy="restaurant")
    private Set<Menu> menu;

    @Id
    @Column(name="idRestaurant")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nom;
    private long nbPlacesMax;


}

package org.example.testkoubaayoussef.DAO.Repo;

import org.example.testkoubaayoussef.DAO.Entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepo extends JpaRepository<Client, Long> {


}

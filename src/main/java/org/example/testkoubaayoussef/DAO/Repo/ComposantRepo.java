package org.example.testkoubaayoussef.DAO.Repo;

import org.example.testkoubaayoussef.DAO.Entities.Client;
import org.example.testkoubaayoussef.DAO.Entities.Composant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComposantRepo extends JpaRepository<Composant, Long> {
}

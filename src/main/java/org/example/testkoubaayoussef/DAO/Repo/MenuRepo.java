package org.example.testkoubaayoussef.DAO.Repo;

import org.example.testkoubaayoussef.DAO.Entities.Client;
import org.example.testkoubaayoussef.DAO.Entities.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuRepo extends JpaRepository<Menu, Long> {
}

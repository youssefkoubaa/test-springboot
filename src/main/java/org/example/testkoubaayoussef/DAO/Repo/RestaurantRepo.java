package org.example.testkoubaayoussef.DAO.Repo;

import org.example.testkoubaayoussef.DAO.Entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RestaurantRepo extends JpaRepository<Restaurant, Long> {
}

package org.example.testkoubaayoussef.RestController;

import lombok.AllArgsConstructor;
import org.example.testkoubaayoussef.DAO.Entities.Client;
import org.example.testkoubaayoussef.Services.IClientService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class ClientRestControllers {
    IClientService iClientService;

    @PostMapping("addclient")
    public Client ajouterClient(@RequestBody Client client) {
        return iClientService.ajouterClient(client);
    }
}

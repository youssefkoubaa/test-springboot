package org.example.testkoubaayoussef.RestController;

import lombok.AllArgsConstructor;
import org.example.testkoubaayoussef.DAO.Entities.Composant;
import org.example.testkoubaayoussef.DAO.Entities.Menu;
import org.example.testkoubaayoussef.Services.IMenuService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/menu")
public class MenuRestControllers {
    IMenuService iMenuService;

    @PostMapping("/add")
    public Menu ajouterComposantsEtMiseAjourPrixTotalMenu(@RequestBody List<Composant> composants, @RequestParam Long idMenu) {
        return iMenuService.ajouterComposantsEtMiseAjourPrixTotalMenu(composants, idMenu);
    }
}

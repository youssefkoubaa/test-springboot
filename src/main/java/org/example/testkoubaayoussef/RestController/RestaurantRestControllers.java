package org.example.testkoubaayoussef.RestController;

import lombok.AllArgsConstructor;
import org.example.testkoubaayoussef.DAO.Entities.Restaurant;
import org.example.testkoubaayoussef.Services.IRestaurantService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class RestaurantRestControllers {
    IRestaurantService restaurantService;

    @PostMapping("restaurant")
    public Restaurant ajouterRestaurantETMenuAssocie(@RequestBody Restaurant restaurant) {
        return restaurantService.ajouterRestaurantETMenuAssocie(restaurant);
    }


}

package org.example.testkoubaayoussef.Services;

import lombok.AllArgsConstructor;
import org.example.testkoubaayoussef.DAO.Entities.Client;
import org.example.testkoubaayoussef.DAO.Repo.ClientRepo;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ClientService implements IClientService{
    ClientRepo clientRepo;


    @Override
    public Client ajouterClient(Client client) {
        return clientRepo.save(client);
    }
}

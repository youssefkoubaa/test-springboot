package org.example.testkoubaayoussef.Services;

import org.example.testkoubaayoussef.DAO.Entities.Composant;
import org.example.testkoubaayoussef.DAO.Entities.Menu;

import java.util.List;

public interface IMenuService {

    Menu ajouterComposantsEtMiseAjourPrixTotalMenu(List<Composant> composants, Long idMenu);
}

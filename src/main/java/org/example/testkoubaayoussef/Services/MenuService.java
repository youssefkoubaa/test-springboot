package org.example.testkoubaayoussef.Services;

import lombok.AllArgsConstructor;
import org.example.testkoubaayoussef.DAO.Entities.Composant;
import org.example.testkoubaayoussef.DAO.Entities.Menu;
import org.example.testkoubaayoussef.DAO.Repo.MenuRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MenuService implements IMenuService {
    MenuRepo menuRepo;

    @Override
    public Menu ajouterComposantsEtMiseAjourPrixTotalMenu(List<Composant> composants, Long idMenu) {

        Menu menuToUpdate = menuRepo.findById(idMenu).orElse(null);
        if (menuToUpdate != null) {
            menuToUpdate.setComposants(composants);
            return menuRepo.save(menuToUpdate);
        }
        return null;
    }
}

package org.example.testkoubaayoussef.Services;

import lombok.AllArgsConstructor;
import org.example.testkoubaayoussef.DAO.Entities.Restaurant;
import org.example.testkoubaayoussef.DAO.Repo.RestaurantRepo;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RestaurantService implements IRestaurantService{
RestaurantRepo restaurantRepo;
    @Override
    public Restaurant ajouterRestaurantETMenuAssocie(Restaurant restaurant){
    return restaurantRepo.save(restaurant);
}
}
